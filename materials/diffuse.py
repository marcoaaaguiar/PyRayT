import numpy as np
from materials.material import Material
from pyrayt import random_unit_sphere
from pyrayt.ray import Ray


class Diffuse(Material):
    def __init__(self, attenuation, world, dispersion=0.9):
        self.attenuation = attenuation
        self.dispersion = dispersion
        self.world = world

    def scatter(self, ray, hitrecord):
        if ray.depth_left > 0:
            new_ray = Ray(hitrecord.point,
                          hitrecord.point + hitrecord.normal + self.dispersion * random_unit_sphere(),
                          depth_left=ray.depth_left - 1)

            return self.attenuation * self.world.color(new_ray)
        else:
            return np.array([0, 0, 0])
