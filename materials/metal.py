import numpy as np
from materials.material import Material
from pyrayt import random_unit_sphere
from pyrayt.ray import Ray


class Metal(Material):
    def __init__(self, attenuation, world, fuzziness=0.0):
        self.attenuation = np.array(attenuation)
        self.world = world
        self.fuzziness = fuzziness

    def scatter(self, ray, hitrecord):
        if ray.depth_left > 0:
            new_ray = Ray(hitrecord.point,
                          self._reflect(ray, hitrecord.normal) + self.fuzziness * random_unit_sphere(),
                          depth_left=ray.depth_left - 1)

            return self.attenuation * self.world.color(new_ray)
        else:
            return np.array([0, 0, 0])

    @staticmethod
    def _reflect(ray, normal):
        return ray.direction - 2 * np.dot(ray.direction, normal) * normal
