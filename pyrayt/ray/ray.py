import numpy as np


class Ray:
    def __init__(self, origin, direction, **kwargs):
        """

        :param origin: Ray center
        :param direction: Camera direction
        :param kwargs:
        """
        self.origin = np.array(origin)
        self._direction = None
        self.direction = direction
        self.color = np.array([0, 0, 0])
        self.depth_left = 10

        for kw, val in kwargs.items():
            setattr(self, kw, val)

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._direction = value / np.linalg.norm(value)

    def point_at_t(self, t):
        return self.origin + t * self.direction
