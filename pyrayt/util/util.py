import numpy as np


def random_unit_sphere():
    vec = 2 * np.random.random(3) - np.array([1, 1, 1])
    if np.linalg.norm(vec) < 1:
        return vec
    else:
        return random_unit_sphere()
