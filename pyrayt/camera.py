import time

from numpy import array
from matplotlib import pyplot as plt

import numpy as np

from pyrayt.ray.ray import Ray


class Camera:
    def __init__(self, position, direction, world, **kwargs):
        """

        :param position: Camera position
        :param direction: Camera direction
        :param World world: World
        :param kwargs:
        """
        self.world = world
        self.position = np.array(position)
        self._direction = None
        self.direction = np.array(direction)
        self._t = 1

        self.samples_per_pixel = 1

        self.screen_width = 4
        self.screen_height = 2
        self.screen_nx = 200
        self.screen_ny = 100
        self.screen_distance = 1
        self.screen_lower_left_corner = self.pointing_point - self.horizontal / 2 - self.vertical / 2

        for kw, val in kwargs.items():
            setattr(self, kw, val)

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._t = np.linalg.norm(value)
        self._direction = value / self._t

    @property
    def pointing_point(self):
        return self.position + self.direction * self._t

    @property
    def horizontal(self):
        return array([self.screen_width, 0, 0])

    @property
    def vertical(self):
        return array([0, self.screen_height, 0])

    def capture(self):
        time_start = time.time()

        img_matrix = np.zeros((self.screen_ny, self.screen_nx, 3))
        for x in range(self.screen_nx):
            if x % 10 == 0:
                print("{:.2f} %".format(x / float(self.screen_nx) * 100))

            for y in range(self.screen_ny):
                # For each super sampling
                color = np.array([0, 0, 0])
                x_rand = np.insert(np.random.rand(self.samples_per_pixel - 1), 0, 0)
                y_rand = np.insert(np.random.rand(self.samples_per_pixel - 1), 0, 0)

                for s in range(self.samples_per_pixel):
                    # Creates the ray
                    scaled_x = (x + x_rand[s]) / self.screen_nx  # scan left to right
                    scaled_y = 1 - (y + y_rand[s]) / self.screen_ny  # scan top to bottom
                    direction = self.screen_lower_left_corner + scaled_x * self.horizontal + scaled_y * self.vertical
                    ray = Ray(origin=self.position, direction=direction)

                    # Ray hit registration and color
                    color = color + self.world.color(ray)

                color = color / self.samples_per_pixel
                img_matrix[y, x, :] = np.clip(color, 0, 1)
        # plt.imsave('fig4.png', img_matrix)
        print("Total time: {}".format(time.time() - time_start))

        img_matrix = self.post_process(img_matrix)
        plt.imshow(img_matrix)
        plt.show()
        return img_matrix

    @staticmethod
    def post_process(img_matrix):
        img_matrix = np.sqrt(img_matrix)
        return img_matrix
