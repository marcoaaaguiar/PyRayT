import numpy as np

from pyrayt.objects.hitable import Hitable
from pyrayt.ray.hitrecord import HitRecord
from pyrayt.ray.ray import Ray


class Sphere(Hitable):
    def __init__(self, center, world, material, radius=1, **kwargs):
        self.center = np.array(center)
        self.radius = radius
        self.world = world
        self.material = material

        # self.color_type = 'normal_heatmap'
        # self.color_type = 'fix'
        self.color_type = 'diffuse'
        self._color = np.array([1, 0, 0])

        for kw, val in kwargs.items():
            setattr(self, kw, val)

    def hit(self, ray):
        """Check if the ray hits the target, if it hits it returns the distance.
        Otherwise, it will return -1.0

        :param ray: Ray
        :return:
        """
        origin_center = ray.origin - self.center
        # a = ray.direction @ ray.direction  # isn't this one if direction is unitary?
        a = 1
        b = origin_center @ ray.direction
        c = (origin_center @ origin_center) - self.radius * self.radius
        discriminant = b * b - a * c
        if 0 < discriminant:
            distance = -b - np.sqrt(discriminant) / a
            if 0.001 < distance:
                normal = (ray.point_at_t(distance) - self.center) / self.radius
                hitrecord = HitRecord(distance, ray.point_at_t(distance), normal)
                return hitrecord

            distance = -b + np.sqrt(discriminant) / a
            if 0.001 < distance:
                normal = (ray.point_at_t(distance) - self.center) / self.radius
                hitrecord = HitRecord(distance, ray.point_at_t(distance), normal)
                return hitrecord

        return None

    def color(self, ray, hitrecord):
        """
        :param Ray ray:
        :param HitRecord hitrecord:
        """
        if self.color_type == 'fixed':
            return self._color
        elif self.color_type == 'normal_heatmap':
            return 0.5 * (hitrecord.normal + 1)
        elif self.color_type == 'diffuse':
            # if ray.depth_left > 0:
            #     new_ray = Ray(hitrecord.point,
            #                   hitrecord.point + hitrecord.normal + random_unit_sphere(),
            #                   depth_left=ray.depth_left - 1)
            #
            #     # closest_obj, closest_hitrecord = self.world.ray_intersect(new_ray)
            #     # return 0.5 * closest_obj.color(new_ray, closest_hitrecord)
            #     return 0.5 * self.world.color(new_ray)
            # else:
            #     return self._color
            return self.material.scatter(ray, hitrecord)
