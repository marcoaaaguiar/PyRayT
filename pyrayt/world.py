import numpy as np


class World:
    def __init__(self, **kwargs):
        self.camera = None
        self.objects = []

        for key in kwargs:
            setattr(self, key, kwargs[key])

    def ray_intersect(self, ray):
        # Detect hit and obtain color:
        closest_obj = None
        closest_hitrecord = None
        closest_distance = np.inf
        for obj in self.objects:
            hitrecord = obj.hit(ray)
            if hitrecord is not None and hitrecord.distance < closest_distance:
                closest_distance = hitrecord.distance
                closest_obj = obj
                closest_hitrecord = hitrecord

        return closest_obj, closest_hitrecord

    @staticmethod
    def _background_color(ray_):
        t = 0.5 * (ray_.direction[1] + 1.0)
        return (1.0 - t) * np.array([1, 1, 1]) + t * np.array([0.5, 0.7, 1.])

    def color(self, ray):
        closest_obj, closest_hitrecord = self.ray_intersect(ray)
        # Obtain color
        if closest_obj is not None:
            color = closest_obj.color(ray, closest_hitrecord)
        else:
            color = self._background_color(ray)
        return color
