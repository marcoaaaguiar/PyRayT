from materials import Diffuse, Metal
from pyrayt.camera import Camera
from pyrayt.objects.sphere import Sphere
from pyrayt.world import World

world = World()

diffuse1 = Diffuse([0.8, 0.3, 0.3], world)
diffuse2 = Diffuse([0.8, 0.8, 0.0], world)
metal1 = Metal([0.8, 0.6, 0.2], world)
metal2 = Metal([0.8, 0.8, 0.8], world, fuzziness=0.5)

world.objects.append(Sphere(center=[0, 0, -1], radius=0.5, world=world, material=diffuse1))
world.objects.append(Sphere(center=[0, -100.5, -1], radius=100, world=world, material=diffuse2))
world.objects.append(Sphere(center=[1, 0, -1], radius=0.5, world=world, material=metal1))
world.objects.append(Sphere(center=[-1, 0, -1], radius=0.5, world=world, material=metal2))

camera = Camera(position=[0, 0, 0], direction=[0, 0, -1], world=world,
                samples_per_pixel=30,
                screen_nx=600, screen_ny=300
                )
world.camera = camera

camera.capture()
